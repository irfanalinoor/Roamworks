package rw.utilities;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HelperFunction {

	public static void WaitForElement(WebDriver webdriver, By byElement, int minutes){
		/*	WebDriverWait wait = new WebDriverWait(webDriver, timeoutInSeconds);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id<locator>));*/
		
		WebDriverWait wait = new WebDriverWait(webdriver, minutes*60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(byElement));
	}
	
	public static boolean isElementPresent(WebDriver webdriver, By byElement){
		boolean result = false;
		try{
			HelperFunction.WaitForSeconds(5);
			result = webdriver.findElement(byElement).isDisplayed();
		}
		catch(Exception ex){
			System.out.println(ex);
		}
		System.out.println("isElementPresent="+result+" for "+byElement.toString());
		return result;
	}
	
	public static void WaitForSeconds(int seconds){
		try{
			Thread.sleep(seconds*1000);
		}
		catch(InterruptedException ex){
			Thread.currentThread().interrupt();
			System.out.println(ex);
		}
	}
	
	public static void ClickElement(WebDriver webdriver, By byElement){
		Assert.assertTrue(isElementPresent(webdriver, byElement));
		webdriver.findElement(byElement).click();
	}
	
	
}//end of class
