package rw.website.pages;
//-----------------
import org.openqa.selenium.WebDriver;
import rw.website.locators.HomePgLoc;


//-----
public class HomePage {

	   private final WebDriver webdriver;
       
       public HomePage(WebDriver driver) {
	        this.webdriver = driver;

	        // Check that we're on the right page.
	        if (!driver.getTitle().equals("Home | ROAMWORKS")) {
	            throw new IllegalStateException("This is not the Home | ROAMWORKS page , current page is " + driver.getTitle());
	        }
	    }
	    
       public boolean isCompanyLogoLoaded(){
    	   return this.webdriver.findElement(HomePgLoc.companylogo_loc).isDisplayed();
       }
       
       public boolean isMainMenusLoaded(){
    	   boolean result = this.webdriver.findElement(HomePgLoc.homemenu_loc).isDisplayed() 
    			   			&&
    			   			this.webdriver.findElement(HomePgLoc.solutionmenu_loc).isDisplayed() 
    			   			&&
    			   			this.webdriver.findElement(HomePgLoc.technologymenu_loc).isDisplayed() 
    			   			&&
    			   			this.webdriver.findElement(HomePgLoc.partnermenu_loc).isDisplayed() 
    			   			&&
    			   			this.webdriver.findElement(HomePgLoc.companymenu_loc).isDisplayed() 
    			   			&&
    			   			this.webdriver.findElement(HomePgLoc.searchbox_loc).isDisplayed() 
    			   			;
    			   			
    	   
    	   return result;
       }
       
       
}
