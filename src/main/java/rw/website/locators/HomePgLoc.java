package rw.website.locators;

import org.openqa.selenium.By;

public final class HomePgLoc {
	
	public static By companylogo_loc = By.xpath(".//*[@id='menu-item-661']/a");
	public static By homemenu_loc = By.xpath(".//*[@id='nav-menu-item-320']/a/span[1]");
	public static By solutionmenu_loc = By.xpath(".//*[@id='nav-menu-item-627']/a/span[1]");
	public static By technologymenu_loc = By.xpath(".//*[@id='nav-menu-item-595']/a/span[1]");
	public static By partnermenu_loc = By.xpath(".//*[@id='nav-menu-item-322']/a/span[1]");
	public static By companymenu_loc = By.xpath(".//*[@id='nav-menu-item-553']/a/span[1]");
	public static By searchbox_loc = By.xpath(".//*[@id='s']");
	
}