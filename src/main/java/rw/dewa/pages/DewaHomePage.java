package rw.dewa.pages;
//-----------------
import org.junit.Assert;

import org.openqa.selenium.WebDriver;

import rw.utilities.HelperFunction;
import rw.dewa.locators.DewaHomePgLoc;
import rw.dewa.locators.LoginPgLoc;


//-----
public class DewaHomePage{

	   private final WebDriver webdriver; // this is class variable 
       
       public DewaHomePage(WebDriver driver) {
	        this.webdriver = driver;
	        HelperFunction.WaitForSeconds(5);
	        HelperFunction.WaitForElement(webdriver, DewaHomePgLoc.topAlarmBar_loc, 3);
	        
	    }
	    
       public void HideTopAlarmBar(){

    	   HelperFunction.ClickElement(webdriver, DewaHomePgLoc.hidetopbar_loc);
    	   HelperFunction.WaitForSeconds(3);
    	   
       }

	
       public void VerifyHomeHeadersLoaded(){
    	   Assert.assertTrue(HelperFunction.isElementPresent(webdriver, DewaHomePgLoc.homeDewaLogo_loc));
    	   Assert.assertTrue(HelperFunction.isElementPresent(webdriver, DewaHomePgLoc.homeDewaMenu_loc));
    	   Assert.assertTrue(HelperFunction.isElementPresent(webdriver, DewaHomePgLoc.homeDewaProfileSection_loc));
    	   // do assertion for rest of header locators
    	   
       }
       
}
