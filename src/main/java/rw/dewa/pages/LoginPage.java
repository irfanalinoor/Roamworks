package rw.dewa.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import rw.dewa.locators.DewaHomePgLoc;
import rw.dewa.locators.LoginPgLoc;
import rw.utilities.HelperFunction;

public class LoginPage {

	private final WebDriver webdriver;
	
	public LoginPage(WebDriver driver) { // this is constructor
        this.webdriver = driver;

        // Check that we're on the right page.
        //if (!webdriver.getTitle().equals("Login [ROAM]")) {
          //  throw new IllegalStateException("This is not the Deva | Login page , current page is " + webdriver.getTitle());
       // }
    }
	
	public boolean isLoginPageLoaded(){
		boolean result = false;
		result = webdriver.findElement(LoginPgLoc.loginHeading_loc).isDisplayed()
				&&
				webdriver.findElement(LoginPgLoc.loginDialog_loc).isDisplayed()
				&&
				webdriver.findElement(LoginPgLoc.usernameBox_loc).isDisplayed()
				&&
				//webdriver.findElement(LoginPgLoc.loginButton_loc).isDisplayed()
				HelperFunction.isElementPresent(webdriver, LoginPgLoc.loginButton_loc);
				;
		
		return result;
	}
	
	
	public DewaHomePage LoginWithCredentials(){
		webdriver.findElement(LoginPgLoc.usernameBox_loc).sendKeys("mobink");
		HelperFunction.WaitForSeconds(3);
		webdriver.findElement(LoginPgLoc.passwordBox_loc).sendKeys("test1234");
		HelperFunction.WaitForSeconds(3);
		HelperFunction.ClickElement(webdriver, LoginPgLoc.loginButton_loc);
		//webdriver.findElement(LoginPgLoc.loginButton_loc).click();
		
		Assert.assertFalse(HelperFunction.isElementPresent(webdriver, LoginPgLoc.errorText));
		return new DewaHomePage(webdriver);		
	}
}
