package rw.dewa.locators;

import org.openqa.selenium.By;

public final class DewaHomePgLoc {
	

		//Header Locators
		public static By homeDewaLogo_loc = By.xpath(".//*[@id='menuBar']/div[1]/a");
		public static By homeDewaMenu_loc = By.xpath(".//*[@id='menu-menu']/a");
		public static By homeDewaAlarm_loc = By.xpath(".//*[@id='menu-alarms']/a");
		public static By homeDewaMenuNotifications_loc = By.xpath(".//*[@id='menu-notifications']/a");
		public static By homeDewaMenuPanels_loc = By.xpath(".//*[@id='menu-layout']/a");
		public static By homeDewaSearchSection_loc = By.xpath(".//*[@id='menuBar']/div[2]/ul[2]/li[1]");
		public static By homeDewaProfileSection_loc = By.xpath(".//*[@id='menuBar']/div[2]/ul[2]/li[2]/a");
		public static By homeDewaSetting_loc = By.xpath(".//*[@id='menuBar']/div[2]/ul[2]/li[3]/a");
		public static By homeDewaHelp_loc = By.xpath(".//*[@id='menuBar']/div[2]/ul[2]/li[4]/a");
		public static By homeDewaLogout_loc = By.xpath(".//*[@id='menuBar']/div[2]/ul[2]/li[5]/a");
		public static By homeDewaMenuBar_loc = By.xpath(".//*[@id='menuBar']/div[2]");
		public static By homeDewaSearchBar_loc = By.xpath(".//*[@id='menuBar']/div[2]/ul[2]/li[1]");
		//Alarm Bar Locators
		public static By hidetopbar_loc = By.xpath(".//*[@id='alarmBar']/div[2]/a/span");
		public static By topAlarmBar_loc = By.xpath(".//*[@id='alarmBar']");

		//
}