package rw.dewa.locators;

import org.openqa.selenium.By;

public final class LoginPgLoc {

	// public static By sample = By.xpath("");
	public static By loginHeading_loc = By.xpath(".//*[@id='loginDialog_wnd_title']");
	public static By loginDialog_loc = By.xpath(".//*[@id='loginDialog']/div");
	public static By usernameBox_loc = By.xpath(".//*[@id='username']");
	public static By passwordBox_loc = By.xpath(".//*[@id='password']");
	public static By loginButton_loc = By.xpath("//div[4]/div[3]/div/button");
	public static By errorText = By.xpath("//*[@id='validation-error']");
}


