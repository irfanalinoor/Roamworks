package rw.website.tests;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class TestBaseClass {

    WebDriver Driver;

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
    }

    @Before
    public void setupTest() {
        String siteUrl = "http://latest.roamworks.com/dewa/caesar";
        Driver = new ChromeDriver();
        Driver.manage().window().maximize();//setSize(new Dimension(1150, 650));
        Driver.navigate().to(siteUrl);
    }

    @After
    public void tearDownTest() {
    	Driver.quit();
    }

}
